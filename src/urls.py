from django.conf.urls import patterns, include, url
from django.contrib import admin
from . import settings
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', 'src.cash.views.mainpage', name= 'mainpage'),
    url(r'^registration/$', 'src.cash.views.registration', name= 'registration'),
    url(r'^logout/$', 'src.cash.views.logout', name= 'logout'),
    url(r'^cash/' , include('src.cash.urls', 'cash')),
)


urlpatterns += staticfiles_urlpatterns()

if settings.DEBUG:
    urlpatterns += patterns('',
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve' , {'document_root': settings.MEDIA_ROOT}),
    )

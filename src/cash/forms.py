# -*- coding: utf-8 -*-

from django import forms
from django.contrib import auth
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from django.utils.translation import ugettext_lazy as _
from .models import User, Transaction, Tag

class AuthenticationForm(forms.Form):
    username = forms.CharField(label=_(u'Логин'), min_length=3, max_length=30)  
    password = forms.CharField(label=_(u'Пароль'), widget=forms.PasswordInput, min_length=6, max_length=30)  

    def clean_username(self):
        value = self.cleaned_data['username']
        is_user = User.objects.filter(username=value).count()
        if not is_user: 
            raise forms.ValidationError(_(u'Такой логин не зарегистрирован!')) 
        return value
 
    def clean_password(self):
        cd = self.cleaned_data
        if not self.errors:
            self._user = auth.authenticate(username=cd['username'], password=cd['password'])
            if self._user is None:
                raise forms.ValidationError(_(u'Неверный пароль!'))
        return cd["password"]


class RegistrationForm(UserCreationForm):

    class Meta:
        model = User
        fields = ('username', 'password1', 'password2', 'currency')
        widgets = {
            'currency': forms.RadioSelect(),
        }

    def clean_username(self):
        username = self.cleaned_data["username"]
        try:
            User._default_manager.get(username=username)
        except User.DoesNotExist:
            return username
        raise forms.ValidationError(self.error_messages['duplicate_username'])
        
class TransactionForm(forms.ModelForm):
    tags = forms.CharField(label= _(u'Теги'), max_length= 80)
    
    class Meta:
        model = Transaction
        fields = ('value', 'date', 'tags', 'note')
        
    def __init__(self, user, data= None, *args, **kwargs):
        dct = dict()
        if data:
            # фикс имен пост атребутов. 
            # в cash/cash.html форма редактирования передает данные с префиксом 'edit_' в имени
            for k,v in data.items():
                dct[k.replace('edit_', '')] = v
        self.user = user
        super(TransactionForm, self).__init__(data= dct or None, *args, **kwargs)
    
    def clean_tags(self):
        value = self.cleaned_data['tags']
        tags = list()
        for tag in value.split(','):
            tags.append(tag.lstrip().rstrip().lower())
        if len(tags) > 5: 
            raise forms.ValidationError(_(u'Нельзя указывать больше 5 тегов!'))
        self.tags = tags 
        return value
            
    def save(self):
        data = self.cleaned_data
        transaction = super(TransactionForm, self).save(commit= False)
        transaction.user = self.user
        transaction.save()
        for tag_title in self.tags:
            try:
                tag = Tag.objects.get(title= tag_title)
            except Tag.DoesNotExist:
                tag = Tag.objects.create(title= tag_title)
            tag.transactions.add(transaction)
        self.user.balans += transaction.value
        self.user.save()
        return transaction            
            
            
            
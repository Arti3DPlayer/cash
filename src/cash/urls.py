# -*- coding: utf-8 -*-

from django.conf.urls import patterns, include, url


urlpatterns = patterns('src.cash.views',
    url(r'^$', 'cash', name= 'cash'),
    url(r'^transaction/(?P<month>\d{2,2})/(?P<year>\d{4,4})/$', 'transaction', name= 'transaction'),
    url(r'^transaction/$', 'transaction_all', name= 'transaction_all'),
    url(r'^transaction/add/$', 'transaction_add', name= 'transaction_add'),
    url(r'^transaction/edit/$', 'transaction_edit', name= 'transaction_edit'),
    url(r'^transaction/delete/$', 'transaction_del', name= 'transaction_del'),
    url(r'^tag/(?P<month>\d{2,2})/(?P<year>\d{4,4})/$', 'tag', name= 'tag'),
    url(r'^tag/$', 'tag_all', name= 'tag_all'),
    url(r'^chart/(?P<month>\d{2,2})/(?P<year>\d{4,4})/$', 'chart', name= 'chart'),
)


